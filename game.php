<?php

function getProfile($userId)
{
    global $db;

    $profileQueryString = "SELECT `points`,`items` FROM `users` WHERE `id` = ?s";

    $user = $db->getRow($profileQueryString, $userId);

    if (!$user) {
        $res = $db->query("INSERT INTO `users` (`id`,`items`) VALUES (?s,'{}')", $userId);

        if (!$res) {
            printError("can not insert user");
            return;
        }

        $user = $db->getRow($profileQueryString, $userId);

        if (!$res) {
            printError("can not get user");
            return;
        }
    }

    $chResult = CoinHive::getBalance($userId);

    if (!$chResult->success && $chResult->error != 'unknown_user') {
        printError('can not get user balance');
        return;
    }

    if ($chResult->balance) {
        $user["balance"] = $chResult->balance;
    } else {

        $user["balance"] = 0;
    }

    $user["items"] = json_decode($user["items"]);

    printResponse($user);
}

function buyItem($userId, $itemId)
{
    global $db;

    $item = Shop::getItemById($itemId);

    if (!$item) {
        printError('no item with id ' . $itemId);
        return;
    }

    $user = $db->getRow("SELECT `points`,`items` FROM `users` WHERE `id` = ?s", $userId);

    if (!$user) {
        printError("user not found");
        return;
    }

    $userItems = (array)json_decode($user["items"]);

    $chResult = CoinHive::getBalance($userId);

    if (!$chResult->success) {
        printError('can not get user balance');
        return;
    }

    if ($chResult->balance) {
        $user["balance"] = $chResult->balance;
    } else {

        $user["balance"] = 0;
    }

    $itemPrice = intval($item["price"]);

    $balance = intval($user["balance"]);

    if ($balance < $itemPrice) {
        printError("not enough money");
        return;
    }

    $balance -= $itemPrice;

    $chResult = CoinHive::withdraw($userId, $itemPrice);

    if (!$chResult->success) {
        printError('can not withdraw user balance');
        return;
    }

    $userItem = $userItems[$itemId];

    if ($userItem) {
        $userItem->count += 1;
    } else {
        $userItems[$itemId] = [
            'count' => 1
        ];
    }

    $pointsAward = $itemPrice;

    $updateResult = $db->query("UPDATE `users` SET `points`=`points`+?i,`items`=?s WHERE `id`=?s",
        $pointsAward,
        json_encode($userItems),
        $userId);


    $user["balance"] = $balance;
    $user["points"] = intval($user["points"]) + $pointsAward;
    $user["items"] = $userItems;

    printResponse($user);
}

function getTop()
{
    global $db;

    $users = $db->getAll("SELECT `name`,`points` FROM `users` ORDER BY `points` DESC LIMIT 10");

    printResponse($users);
}