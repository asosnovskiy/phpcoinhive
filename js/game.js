const Game = {
    siteKey: "KqNONX9oxfVraMCkuewW6651VAweDTie",
    shop: null,
    items: null,
    tokenLength: 64,
    minerThrottle: 0.1
};

Game.createToken = function () {
    const tokenLength = Game.tokenLength;

    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < tokenLength; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

Game.checkToken = function () {
    var query = location.search.substr(1);
    var result = {};
    query.split("&").forEach(function (part) {
        var item = part.split("=");
        result[item[0]] = decodeURIComponent(item[1]);
    });

    if (result.token) {
        console.log("token from url:" + result.token);
        localStorage.setItem('token', result.token);
    }
};

Game.token = function () {
    var savedToken = localStorage.getItem('token');

    if (!savedToken) {
        savedToken = Game.createToken();
        localStorage.setItem('token', savedToken);
    }

    return savedToken;
};

Game.refreshShop = function () {
    const shopItems = $("#shop-items");

    if (!Game.shopItemView)
        Game.shopItemView = $(".shop-item").clone();

    const shopItem = Game.shopItemView;

    shopItems.html('');

    for (var itemId in Game.shop) {
        const itemData = Game.shop[itemId];

        const newShopItem = shopItem.clone();

        const id = itemId;

        $(newShopItem).css("background-image", "url(./img/" + itemId + ".png)");

        $(newShopItem).find('.shop-item-name').text(itemData.name);
        $(newShopItem).find('.shop-item-price').text(itemData.price + "$");

        $(newShopItem).find('.shop-item-buy').click(function () {
            Game.buyItem(id);
        });

        newShopItem.appendTo(shopItems);
    }
};

Game.refreshProfile = function () {
    const profileItems = $("#profile-items");

    if (!Game.profileItemView)
        Game.profileItemView = $(".profile-item").clone();

    const shopItem = Game.profileItemView;

    profileItems.html('');

    $("#ubalance").text("Balance:" + Game.balance + "$");

    if (!Game.shop)
        return;

    for (var itemId in Game.items) {
        const shopData = Game.shop[itemId];
        const itemData = Game.items[itemId];

        const newProfileItem = shopItem.clone();

        $(newProfileItem).find('.profile-item-name').text("Name:" + shopData.name);
        $(newProfileItem).find('.profile-item-count').text("Count:" + itemData.count);

        newProfileItem.appendTo(profileItems);
    }
};

Game.updateBalance = function (cb) {
    Api.getProfile(Game.token(), function (resp) {
        if (!resp.success) {
            return console.log(resp);
        }

        Game.balance = resp.data.balance;
        Game.items = resp.data.items;

        Game.refreshProfile();

        cb ? cb() : null;
    });
};

Game.updateShop = function (cb) {
    Api.getShop(Game.token(), function (resp) {
        if (!resp.success) {
            return console.log(resp);
        }

        Game.shop = resp.data;

        Game.refreshShop();

        cb ? cb() : null;
    });
};

Game.buyItem = function (itemId, cb) {
    Api.buyItem(Game.token(), itemId, function (resp) {
        if (!resp.success) {
            return console.log(resp);
        }

        Game.balance = resp.data.balance;
        Game.items = resp.data.items;

        Game.refreshProfile();

        cb ? cb() : null;
    });
};

Game.startMining = function (token) {
    if (Game.miner) {
        Game.stopMining();
    }

    Game.miner = new CoinHive.User(Game.siteKey, token, {
        throttle: Game.minerThrottle
    });

    Game.miner.on('accepted', function () {
        Game.updateBalance();
    });

    Game.miner.start();
};

Game.stopMining = function () {
    if (Game.miner) {
        Game.miner.stop();
        Game.miner = null;
    }
};

$(document).ready(function () {
    console.log("ready!");

    Game.checkToken();

    $("#utoken").val(Game.token());

    Game.updateShop(function () {
        Game.updateBalance();
    });

    Game.startMining(Game.token());
});