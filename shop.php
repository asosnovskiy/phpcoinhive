<?php

class Shop
{
    static $shop = [
        'i1' => [
            'name' => "Board",
            'price' => 10000
        ],
        'i2' => [
            'name' => "Ladder",
            'price' => 50000
        ]
    ];

    static function getItemById($id)
    {
        return Shop::$shop[$id];
    }
}