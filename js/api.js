const Api = {
    baseUrl: 'https://devby.ru/ch/api.php'
};

Api.sendRequest = function (token, method, data, callback) {
    data = data || {};

    data.token = token;
    data.cmd = method;

    $.ajax({
        url: Api.baseUrl,
        type: 'POST',
        data: data,
        success: function (response) {
            callback(response);
        },
        error: function (er) {
            var resp = {
                success: false,
                error: er
            };

            callback(resp);
        }
    });
};

Api.getProfile = function (token, cb) {
    Api.sendRequest(token, 'profile', {}, cb);
};

Api.getShop = function (token, cb) {
    Api.sendRequest(token, 'getShop', {}, cb);
};

Api.buyItem = function (token, itemId, cb) {
    Api.sendRequest(token, 'buyItem', {itemId: itemId}, cb);
};