<?php

function printError($message)
{
    echo json_encode(['success' => false, 'error' => $message]);
}

function printResponse($message)
{
    echo json_encode(['success' => true, 'data' => $message], JSON_NUMERIC_CHECK);
}