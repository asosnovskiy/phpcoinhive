<?php

class CoinHive
{
    const BASE_URL = "https://api.coinhive.com/";
    const SECRET = "EjeiQxIMz4U5k9mgYH16W8p0m7xaIync";

    static function makeRequestUrl($method)
    {
        $url = CoinHive::BASE_URL . $method;

        return $url;
    }

    static function sendRequest($method, $pars, $type = "GET")
    {
        $file = null;
        $url = CoinHive::makeRequestUrl($method);

        $pars['secret'] = CoinHive::SECRET;

        if ($type == "GET") {
            $url .= "?";
            foreach ($pars as $key => $val) {
                $url .= "&" . $key . "=" . $val;
            }

            $file = file_get_contents($url);
        } else {
            $opts = ['http' =>
                [
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query($pars)
                ]
            ];

            $context = stream_context_create($opts);

            $file = file_get_contents($url, false, $context);
        }

        return json_decode($file);
    }

    static function getBalance($userId)
    {
        $res = CoinHive::sendRequest('user/balance', [
            'name' => $userId
        ]);

        return $res;
    }

    static function withdraw($userId, $amount)
    {
        $res = CoinHive::sendRequest('user/withdraw', [
            'name' => $userId,
            'amount' => $amount
        ], "POST");

        return $res;
    }
}