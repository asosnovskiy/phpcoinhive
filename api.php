<?php

require 'config.php';
require 'db.php';
require 'coinhive.php';
require 'func.php';
require 'shop.php';
require 'game.php';

header('Content-type:application/json;charset=utf-8');
header('Access-Control-Allow-Origin:*');

$req = $_REQUEST;

$cmd = $req["cmd"];
$token = $req["token"];

if (!$token) {
    printError('no token');
    return;
}

switch ($cmd) {
    case "profile":
        getProfile($token);
        break;
    case "buyItem":
        $itemId = $req["itemId"];
        buyItem($token, $itemId);
        break;
    case "getShop":
        printResponse(Shop::$shop);
        break;
    case "getTop":
        getTop();
        break;
    default:
        printError('no handler');
        return;
}


