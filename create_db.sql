CREATE TABLE users
(
  id       VARCHAR(64)                     NOT NULL,
  points   INT DEFAULT '0'                 NOT NULL,
  items    JSON                            NOT NULL,
  name     VARCHAR(32) DEFAULT 'Anonymous' NOT NULL,
  publicId INT AUTO_INCREMENT
    PRIMARY KEY,
  refId    INT                             NULL,
  CONSTRAINT users_id_uindex
  UNIQUE (id)
);