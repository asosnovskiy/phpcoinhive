<?php

function config($key = '')
{
    $config = [
        'dbHost' => '127.0.0.1',
        'dbPort' => 3306,
        'dbUser' => 'root',
        'dbPass' => '',
        'dbName' => 'coinhive',
    ];
    return isset($config[$key]) ? $config[$key] : null;
}