<?php

require 'db.class.php';

$db = new SafeMySQL([
    'host' => config("dbHost"),
    'user' => config("dbUser"),
    'pass' => config("dbPass"),
    'db' => config("dbName"),
    'port' => config("dbPort"),
    'charset' => 'utf8',
]);